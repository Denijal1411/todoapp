﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class DAL : DoItem
    {
        public static ToDoEntities Contexts = new ToDoEntities();
        public static void ChangeToDO(string text) {
            Contexts.DoItems.Add(new DoItem()
            {
                Item = text,
                Active = true,
                DateAdded = DateTime.Now
            });
            Contexts.SaveChanges();
        }
        public static List<DoItem> GetAllToDO() {
            DateTime dateSearch = DateTime.Now.AddDays(-30);
            var a=Contexts.DoItems.Where(x => x.DateAdded > dateSearch).OrderByDescending(x => x.Active);
            return a.ToList();
        }


    }
}
