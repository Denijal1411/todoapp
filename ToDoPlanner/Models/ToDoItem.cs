﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToDoPlanner.Models
{
    public class ToDoItems
    {
        public ToDoItems()
        {

        }
        public int ID { get; set; }
        public string Item { get; set; }
        public bool Active{ get; set; }
        public DateTime? DateAdded { get; set; }
         
    }
}