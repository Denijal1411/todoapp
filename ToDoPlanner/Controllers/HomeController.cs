﻿using Data;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToDoPlanner.Models;

namespace ToDoPlanner.Controllers
{
     
    public partial class HomeController : Controller
    { 
        public virtual ActionResult Index(int? page)
        { 
            List<ToDoItems> lista = new List<ToDoItems>();
            var a = DAL.GetAllToDO();

            int pageSize = 15;
            int pageNumber = (page ?? 1);
            //ViewBag.items = a;
            return View(a.ToPagedList(pageNumber, pageSize));
        }
        public virtual ActionResult Done(int id)
        {
            var a = DAL.Contexts.DoItems.FirstOrDefault(x => x.ID == id);
            a.Active = false;
            DAL.Contexts.SaveChanges();
            
            return RedirectToAction("Index", "Home");
        }
        public virtual ActionResult AddtoDoItem(string text)
        {

            DAL.ChangeToDO(text);
            
            return RedirectToAction("Index", "Home");

        }
        public virtual ActionResult FirstPage() {
            return View();
        }


    }
}